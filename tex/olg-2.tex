\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Households}
  \begin{itemize}
  \item One homogenous good.
  \item Each household lives two periods.
  \item In period $t$, the population is composed of $N_t$ young
    households and $N_{t-1}$ old households.
  \item[$\Rightarrow$] $N_t$ is the number of births in period $t$.
  \item[$\Rightarrow$] $P_t = N_t+N_{t-1}$ is the population in period $t$.
  \item Households are productive in period one only.
  \item[$\Rightarrow$] They must save in period 1 to consume in period 2.
  \item Each young household offers inelastically one unit of labor.
  \item $N_t$ is the quantity of labor supply in period $t$.
  \item No heterogeneity among households born in period $t$.
  \item Young household are price takers.
  \item Only young households take decisions.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Representative young household behavior}
  Each time $t$ young household solves the following problem:
  \begin{equation}\label{P:Young}
  \begin{split}
    \max_{\{c_{t,1},c_{t,2}\}} &u(c_{t,1})+\beta u(c_{t,2})\\
    \underline{\text{s.t.}}&\quad c_{t,1} +  \frac{c_{t,2}}{1+r_{t+1}}\leq w_t
  \end{split}
  \end{equation}
  \begin{itemize}
  \item $u()$ is continuous, differentiable, increasing and concave.
  \item $w_t$ is the wage for one unit of labor in period 1.
  \item $R_{t+1}\equiv 1+r_{t+1}$ is the remuneration, in period 2, for one unit of
    good saved in period 1.
  \item $R_{t+1}^{-1}$ is the relative price of consumption in second period.
  \item $s_t = w_t-c_{t,1}$ are the savings of a young household.
  \item $0<\beta<1$ is the discount factor.
  \end{itemize}
\end{frame}


\begin{notes}
  The two periods budgetary constraint in the previous optimization program is the
  aggregation of two budgetary constraints:
  \[
  \begin{cases}
    c_{t,1} + s_t&\leq w_t\\
    c_{t,2} &\leq (1+r_{t+1})s_t
  \end{cases}
  \]
  \[
  \Leftrightarrow\begin{cases}
    c_{t,1} + s_t&\leq w_t\\
    \frac{c_{t,2}}{1+r_{t+1}} &\leq s_t
  \end{cases}
  \]
  By substituting the second constraint in the first one:
  \[
  c_{t,1} +  \frac{c_{t,2}}{1+r_{t+1}}\leq w_t
  \]
  we obtain the two periods budgetary constraint. Given prices, $w_t$
  and $R_{t+1}\equiv 1+r_{t+1}$, this  constraint defines the set of
  feasible allocations.
  \clearpage
\end{notes}

\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Representative young household behavior}
  The Lagrangian associated to this program is:
  \[
  \mathcal L = u(c_{t,1})+\beta u(c_{t,2}) + \lambda_t \left(w_t -
    c_{t,1} - \frac{c_{t,2}}{1+r_{t+1}}\right)
  \]
  Kuhn and Tucker conditions are:
  \begin{equation}
    \label{eq:household:foc:1}\tag{1:a}
    u'(c_{t,1}) = \lambda_t
  \end{equation}
  \begin{equation}
    \label{eq:household:foc:2}\tag{1:b}
    \beta u'(c_{t,2}) = \lambda_t R_{t+1}^{-1}
  \end{equation}
  \begin{equation}
    \label{eq:household:foc:3}\tag{1:c}
    c_{t,1}+R_{t+1}^{-1}c_{t,2}\leq w_t\text{ and } 0 \leq \lambda_t
  \end{equation}
  \begin{equation}
    \label{eq:household:foc:4}\tag{1:d}
    \lambda_t\left(w_t-c_{t,1}-R_{t+1}^{-1}c_{t,2}\right) = 0
  \end{equation}

\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Representative young household behavior}
  \begin{block}{Euler equation}
    The optimal inter-temporal allocation for
    consumption must satisfy:
    \[
    \frac{u'(c_{t,1})}{u'(c_{t,2})} = \beta R_{t+1}
    \]
  \end{block}

  \bigskip

  \textbf{Intuition:} Suppose that the young household chooses to
  postpone an arbitrary small amount of consumption in period 1.
  \begin{itemize}
  \item The loss of utility in period 1 will be $u'(c_{t,1})\mathrm dc_{t,1}$.
  \item In period 2 he will buy $R_{t+1}\mathrm dc_{t,1}$ additional units of good.
  \item The associated gain in utility will be $u'(c_{t,2})R_{t+1}\mathrm dc_{t,1}$
  \item Which evaluated in period 1 is $\beta u'(c_{t,2})R_{t+1}\mathrm dc_{t,1}$
  \item At the optimum the gain and loss must be equal $\Rightarrow$
    Euler.

  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Representative young household behavior (logarithmic utility)}
  \begin{itemize}
  \item Suppose that $u(c)=\log c$, the Euler equation becomes:
    \[
    \frac{c_{t,2}}{c_{t,1}} = \beta R_{t+1}
    \]
  \item By substitution into the two periods budgetary
    constraint (with $s_t=c_{t,2}$):
      \[
      c_{t,1} = \frac{w_t}{1+\beta}
      \]
  \item Because $s_t = w_t-c_{t,1}$, we also have:
    \[
    s_t = \frac{\beta}{1+\beta}w_t
    \]
  \item The saving rate is \emph{constant} as in the Solow model,
    and given by $\mathcal{s} =
    \nicefrac{\beta}{(1+\beta)}=\nicefrac{1}{(2+\rho)}$.
  \bigskip
  \item The saving decision does not depend on $R_{t+1}$, this is
    specific to this utility function.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Representative young household behavior (CRRA utility)}
  \begin{itemize}
  \item Suppose that $u(c)=\frac{c^{1-\theta}-1}{1-\theta}$, with
    $\sigma = \nicefrac{1}{\theta}>0$ the intertemporal elasticity of
    substitution.

  \item The Euler equation becomes:
    \[
    \frac{c_{t,2}}{c_{t,1}} = \left(\beta R_{t+1}\right)^{\sigma}
    \]
  \item By substitution into the two periods budgetary
    constraint (with $s_t=c_{t,2}$):
      \[
      c_{t,1} = \left(1+\beta^{\sigma}R_{t+1}^{\sigma-1}\right)^{-1}w_t
      \]
  \item Because $s_t = w_t-c_{t,1}$, we also have:
    \[
    s_t = \frac{\beta^\sigma R_{t+1}^{\sigma-1}}{1+\beta^\sigma R_{t+1}^{\sigma-1}}w_t
    \]
  \item The saving decision depends on $R_{t+1}$, the saving rate
    $\mathcal s_t$ is not constant anymore.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Representative young household behavior (summary)}
  \begin{itemize}
  \item Define the saving function $s_t = s(w_t,R_{t+1})$
  \item The partial derivates satisfy:
    \[
    s_w(w_t,R_{t+1}) \in (0,1)
    \]
    \[
    s_R(w_t,R_{t+1})\quad
    \begin{cases}
      <0\text{, if } \sigma<1\\
      =0\text{, if } \sigma=1\\
      >0\text{, if } \sigma>1\\
    \end{cases}
    \]
  \item $\mathrm dR>0$
    \begin{itemize}
    \item[$\Rightarrow$] Substitution effect (relatively more
    expensive to consume when young) and Income effect (savings will
    yield more).
  \item[$\Leftrightarrow$] Incentives for increasing $s$ and lowering
    $s$.
  \end{itemize}
  \bigskip
  \item The substitution effect dominates the income effect if and
    only if the elasticity of intertemporal substitution is high
    enough ($\sigma>1$).
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Producer characteristics}
  \begin{itemize}
  \item An infinitely living representative firm rents capital and labor to the
    youngs, and produces the homogeneous good.
    \bigskip
  \item Technology is given by  $Y_t = F(K_t,L_t)$, a neoclassical
    production function:
    \bigskip
    \begin{itemize}
    \item Constant returns to scale:
      \[
      F(\lambda K, \lambda L) = \lambda F(K,L)\quad\quad \forall \lambda>0
      \]
    \item Marginal productivities are positive:
      \[
      F_K(K,L)\geq 0, \quad F_L(K,L)\geq 0
      \]
    \item Marginal productivities are decreasing:
      \[
      F_{KK}(K,L)\geq 0, \quad F_{LL}(K,L)\geq 0
      \]
    \item Inada conditions:
      \[
      \lim_{K\rightarrow 0} F_K(K,L) = \lim_{L\rightarrow 0} F_L(K,L)
      = \infty
      \]
      \[
      \lim_{K\rightarrow \infty} F_K(K,L) = \lim_{L\rightarrow\infty} F_L(K,L)
      = 0
      \]
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Producer characteristics}
  \begin{itemize}
  \item \textbf{Production per worker.} Constant returns to scale implies that we can rewrite the
    production function in intensive form: $Y_t = L_tf(k_t)$,
    where $k_t = \nicefrac{K_t}{L_t}$ is the per worker stock of physical
    capital, and $f(k_t) = F(k_t,1)$ is the productivity of
    labor.
    \bigskip
  \item \textbf{No free lunch.} All the inputs are necessary to the
    production:
    \[
    F(K,0) = F(0,L) = 0 \quad \forall (K,L)\in \mathbb R_{+}^*\times
    \mathbb R_{+}^*
    \]
  \item The marginal productivity of physical capital per worker is
    positive and decreasing:
    \[
    F_K(k_t,1) = f'(k_t) \geq 0
    \]

  \item The marginal productivity of labor is positive and
    decreasing:
    \[
    F_L(K,L) = f(k)-kf'(k) \geq 0
    \]
    \end{itemize}
\end{frame}

\begin{notes}
  \textbf{No free lunch.} By the L'hôpital rule and the Inada
  conditions we know that the productivity of capital goes to zero as
  $K$ goes to infinity:
  \[
  \lim_{K\rightarrow\infty}\frac{Y}{K} =
  \lim_{K\rightarrow\infty}F_K(K,L) = 0
  \]
  Because of the constant returns to scale assumption, we also have
  (given a fixed amount of labor $L$):
  \[
  \lim_{K\rightarrow\infty}\frac{Y}{K} = \lim_{K\rightarrow\infty}
  F\left(1,\frac{L}{K}\right) = F\left(1,0\right) = 0
  \]
  Multiplying both sides of the last equality by $K$, by the constant
  returns to scale assumption, we finally obtain:
  \[
  F(K,0) = 0
  \]
  Following the same approach one can easily establish that $F(0,L) =
  0$, so that both inputs are essential to production.\newline

  \bigskip

  \textbf{Marginal productivity of labor.}  From $F(K,L)=LF(\nicefrac{K}{L},1)$, we have
    \[
    F_L(K,L) = F\left(\frac{K}{L},1\right)-L\frac{K}{L^2}F_K(K,L)
    \]
    and by homogeneity of degree 0 of the marginal productivities
    (remember that if a function is homogenous of degree $h$ then its
    derivate is a function homogenous of degree $h-1$):
    \[
    F_L(K,L) = f(k)-kf'(k) \geq 0
    \]
    \clearpage
\end{notes}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Producer behavior}
  \begin{itemize}

  \item Given prices, $w_{t}$ and $r_{t+1}$,the representative firm
    seeks to maximize its profit:
    \begin{equation}\label{P:Firm}
    \max_{\{K_t,L_t\}} \Pi(K_t,L_t) = f(K_t,L_t) - w_tL_t - (r_{t}+\delta)K_{t}
    \end{equation}
    where $\delta\in[0,1]$ is the depreciation rate of the physical
    capital stock.

    \bigskip

  \item At the optimum of the firm, the marginal productivities must
    be equal to the prices:
    \[
    \begin{split}
      f'(k_t) -\delta &= r_{t}\\
      f(k_t)-k_tf'(k_t) &= w_t
    \end{split}
    \]
  \end{itemize}
\end{frame}


\begin{notes}
  Note that in the first order conditions of the representative firm
  we have two equations, two given prices and an unknown variable: the
  physical capital stock per worker. These conditions do not determine
  the levels of $K$ and $L$ but the ratio ($k=K/L$). This is a
  consequence of the homogeneity of degree one of the production
  function (constant returns to scale) which implies the homogeneity
  of degree zero of the marginal productivities.\newline

  Given $L$, the first equation determine $k_t$ by equating the net
  marginal productivity of the physical capital with the real interest
  rate $r_t$. The second equation ensures that, for any quantity of
  labor $L$, the profit:
  \[
  \Pi(K_t,L_t) = L(t)\left(f(k_t) - w_t - (r_{t}+\delta)k_{t}\right)
  \]
  is equal to zero. The quantity of labor will be determined later at
  the equilibrium.\newline

  
\end{notes}

\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Aggregates}
  \begin{itemize}

  \item Demography: Constant growth rate of the population
    \[
    N_t = (1+n) N_{t-1}
    \]
  \item Total consumption in period $t$ is
    \[
    C_t = N_tc_{t,1} + N_{t-1}c_{t-1,2}
    \]
  \item Gross investment in period $t$ is
    \[
     K_{t+1}-(1-\delta)K_t = I_t = Y_t - C_t
    \]
  \item Youngs' saving in period $t$ is
    \[
    S_t = N_ts(w_t,R_{t+1})
    \]
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Equilibrium}

  \begin{block}{Definition}
    An intertemporal competitive equilibrium is a sequence of prices
    $\{w_t,R_{t+1}\}_{t\in\mathbb N}$, a sequence of consumption
    allocations and input demands $\{c_{t,1},c_{t,2}, K_t,
    L_t\}_{t\in\mathbb N}$, such that:
    \begin{enumerate}[(a)]
    \item Given prices $\{w_t,R_{t+1}\}_{t\in\mathbb N}$, $\{c_{t,1},c_{t,2}\}_{t\in\mathbb N}$ solves \eqref{P:Young}
    \item Given prices $\{w_t,R_{t+1}\}_{t\in\mathbb N}$, $\{K_t,L_t\}_{t\in\mathbb N}$ solves \eqref{P:Firm}
    \item Production factor markets clear for all $t$, $L_t = N_t$ and
      \[
      K_{t+1} = N_t s (w_t, R_{t+1})
      \]
    \end{enumerate}
  \end{block}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Equilibrium}

  \begin{itemize}
  \item The physical capital stock per capita is given by:
    \[
    k_{t+1} = \frac{s(w_t,R_{t+1})}{1+n}
    \]
    where the saving function, $s(,)$, is derived from the optimal
    behavior of the young households.
  \item Substituting the optimality conditions of the representative firm:
    \[
    k_{t+1} = \frac{s(f(k_t)-k_tf'(k_t),1-\delta+f'(k_{t+1}))}{1+n}
    \]
    The law of motion for the physical capital stock per capita in the
    two periods OLG model.
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Logarithmic utility and Cobb-Douglas technology}

  If $u(c) = \log c$ and $y = k^{\alpha}$, the law of motion for the
  per capita stock of physical capital becomes:
  \[
  k_{t+1} = \frac{\beta}{1+\beta}\frac{1-\alpha}{1+n}k_t^{\alpha}
  \]
  \bigskip
  \begin{itemize}
  \item $\exists !$ positive steady state:
    \[
    k^{\star} = \left(\frac{\beta}{1+\beta}\frac{1-\alpha}{1+n}\right)^{\frac{1}{1-\alpha}}
    \]
  \item This steady state is globally stable (decreasing relationship
    between the variation of $k$ and its initial level).
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Logarithmic utility and Cobb-Douglas technology}
  \begin{itemize}
  \item Steady state of output per capita is directly deduced from the
    intensive form of the technology:
    \[
    y^{\star} = \left(\frac{\beta}{1+\beta}\frac{1-\alpha}{1+n}\right)^{\frac{\alpha}{1-\alpha}}
    \]
  \item Note that neither the long run levels of per capital
    production or per capita physical capital stock depends on the
    depreciation rate.
  \item In a two periods OLG model without altruism, the physical capital is not a
    stock but a flow.
    \bigskip
  \item[$\Rightarrow$] It makes sense to calibrate $\delta$ to one.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{Logarithmic utility and Cobb-Douglas technology}
  \begin{itemize}

  \item One can show that the steady state consumption per capita is given by:
    \[
    c^{\star} = \frac{1-\alpha}{1+\beta}\frac{2-\delta+n+\alpha\frac{y^{\star}}{k^{\star}}}{1+n}y^{\star}
    \]
    where $\alpha \left(\nicefrac{y^{\star}}{k^{\star}}\right)$ is the
    steady state level of the marginal productivity of capital.

    \bigskip

  \item An increase in $\delta$ lowers the long run level of
    consumption because more resources must be devoted for replacing
    depreciated capital.
  \end{itemize}

\end{frame}

\begin{notes}
  Consumption per capita is given by:
  \[
  \begin{split}
    c_t &= c_{t,1} + \frac{c_{t,2}}{1+n}\\
    &= w_t-s(w_t,R_{t+1}) +\frac{1+r_{t}}{1+n}s\left(w_{t-1},R_{t}\right)\\
    &= f(k_t)-k_tf'(k_t) - s\left(f(k_t)-k_tf'(k_t),f'(k_{t+1})+1-\delta\right)\\
    &\hspace{1.97cm} +\frac{f'(k_{t})+1-\delta}{1+n}s\left(f(k_{t-1})-k_{t-1}f'(k_{t-1}),f'(k_{t})+1-\delta\right)
  \end{split}
  \]
  If the utility is logarithmic, the previous expression for $c_t$
  simplifies into:
  \[
  c_t = \frac{f(k_t)-k_tf'(k_t)}{1+\beta} + \frac{f'(k_{t})+1-\delta}{1+n}\frac{f(k_{t-1})-k_{t-1}f'(k_{t-1})}{1+\beta}
  \]
  If the production function is Cobb-Douglas, the expression for $c_t$
  simplifies further into:
  \[
  c_t = \frac{1-\alpha}{1+\beta}y_t + \frac{\alpha\frac{y_t}{k_t}+1-\delta}{1+n}\frac{1-\alpha}{1+\beta}y_{t-1}
  \]
  where $\alpha \left(\nicefrac{y_t}{k_t}\right)$ is  the marginal
  productivity of capital in period $t$.

\end{notes}


\begin{frame}
  \frametitle{Basic OLG model with two generations}
  \framesubtitle{CRRA utility and Cobb-Douglas technology}

  If $u(c) = u(c)=\frac{c^{1-\theta}-1}{1-\theta}$ and $y = k^{\alpha}$, the law of motion for the
  per capita stock of physical capital becomes:

\end{frame}

\begin{notes}
  We have:
  \[
  \begin{split}
    (1+n)k_{t+1} &= s(f(k_t)+k_tf'
(k_t), 1-\delta+f'(k_{t+1}))\\
    &= \frac{\beta^{\sigma}\left(1-\delta+\alpha k_{t+1}^{\alpha-1}\right)^{\sigma-1}}{1+\beta^{\sigma}\left(1-\delta+\alpha k_{t+1}^{\alpha-1}\right)^{\sigma-1}}
    \left[k_{t}-\alpha k_t k_{t}^{\alpha-1}\right]\\
    &= \frac{\beta^{\sigma}\left(1-\delta+\alpha k_{t+1}^{\alpha-1}\right)^{\sigma-1}}{1+\beta^{\sigma}\left(1-\delta+\alpha k_{t+1}^{\alpha-1}\right)^{\sigma-1}}
    \left[k_{t}-\alpha k_{t}^{\alpha}\right]\\
  \end{split}
  \]
  which implicitly defines a recurrence for $k$. Suppose that the physical capital stock fully depreciates in one
  period ($\delta=1$), then we have:
  \[
    (1+n)k_{t+1}= \frac{\beta^{\sigma}\alpha^{\sigma-1}k_{t+1}^{(\alpha-1)(\sigma-1)}}{1+\beta^{\sigma}\alpha^{\sigma-1}k_{t+1}^{(\alpha-1)(\sigma-1)}}
    \left[k_{t}-\alpha k_{t}^{\alpha}\right]
  \]

\end{notes}



\end{document}

% Local Variables:
% TeX-master: "slides.tex"
% ispell-check-comments: exclusive
% ispell-local-dictionary: "american-insane"
% End: